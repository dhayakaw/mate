#ifndef SiliconHit_h
#define SiliconHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4Threading.hh"

/// Calorimeter hit class
///
/// It defines data members to store the the energy deposit and track lengths
/// of charged particles in a selected volume:
/// - fEdep, fTrackLength

class SiliconHit : public G4VHit
{
  public:
    SiliconHit();
    SiliconHit(const SiliconHit&);
    virtual ~SiliconHit();

    // operators
    const SiliconHit& operator=(const SiliconHit&);
    G4int operator==(const SiliconHit&) const;

    inline void* operator new(size_t);
    inline void  operator delete(void*);

    // methods from base class
    virtual void Draw() {}
    virtual void Print();

    // methods to handle data
    void SetEdep(G4double de) {fEdep = de;};
    void SetCopyNumber(G4int cn) {fCopyNumber = cn;};

    // get methods
    G4double GetEdep() {return fEdep;};
    G4int GetCopyNumber() {return fCopyNumber;};
      
  private:
    G4double fEdep;        ///< Energy deposit in the sensitive volume
    G4int    fCopyNumber;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

using SiliconHitsCollection = G4THitsCollection<SiliconHit>;

extern G4ThreadLocal G4Allocator<SiliconHit>* SiliconHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* SiliconHit::operator new(size_t)
{
  if (!SiliconHitAllocator) {
    SiliconHitAllocator = new G4Allocator<SiliconHit>;
  }
  void *hit;
  hit = (void *) SiliconHitAllocator->MallocSingle();
  return hit;
}

inline void SiliconHit::operator delete(void *hit)
{
  if (!SiliconHitAllocator) {
    SiliconHitAllocator = new G4Allocator<SiliconHit>;
  }
  SiliconHitAllocator->FreeSingle((SiliconHit*) hit);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

