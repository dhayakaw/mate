#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"

#include "SiliconHit.hh"

#include "globals.hh"

/// Event action class
///
/// In EndOfEventAction(), it prints the accumulated quantities of the energy 
/// deposit and track lengths of charged particles in Absober and Gap layers 
/// stored in the hits collections.

class EventAction : public G4UserEventAction
{
public:
  EventAction();
  virtual ~EventAction();

  virtual void  BeginOfEventAction(const G4Event* event);
  virtual void    EndOfEventAction(const G4Event* event);
    
private:
  // methods
  SiliconHitsCollection* GetHitsCollection(G4int hcID,
                                            const G4Event* event) const;
  // data members                   
  G4int  fAbsHCID;
 // G4int  fGapHCID;
};
                     
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

