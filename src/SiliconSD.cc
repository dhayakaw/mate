#include "SiliconSD.hh"
#include "Analysis.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiliconSD::SiliconSD(
  const G4String& name, 
  const G4String& hitsCollectionName)
 : G4VSensitiveDetector(name),
   fHitsCollection(nullptr),
   m_totalEdep{0.}
{
  collectionName.insert(hitsCollectionName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiliconSD::~SiliconSD() 
{ 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SiliconSD::Initialize(G4HCofThisEvent* hce)
{
  // Create hits collection
  fHitsCollection = new SiliconHitsCollection(SensitiveDetectorName, collectionName[0]); 

  // Add this collection in hce
  auto hcID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  hce->AddHitsCollection( hcID, fHitsCollection ); 

  m_totalEdep[0]=0.;
  m_totalEdep[1]=0.;
  // m_totalEdep[2]=0.;
  // m_totalEdep[3]=0.;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool SiliconSD::ProcessHits(G4Step* step, G4TouchableHistory*)
{  
  // energy deposit
  auto edep = step->GetTotalEnergyDeposit();
  auto copyNumber = step->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber();
  
  if ( edep==0. ) return false;      

  auto newHit = new SiliconHit();
  newHit->SetEdep(edep);
  newHit->SetCopyNumber(copyNumber);
  fHitsCollection->insert(newHit);

  m_totalEdep[copyNumber] += edep;
      
  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SiliconSD::EndOfEvent(G4HCofThisEvent*)
{
  // get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  // fill ntuple
  analysisManager->FillNtupleDColumn(0, m_totalEdep[0]);
  analysisManager->FillNtupleDColumn(1, m_totalEdep[1]);
  // analysisManager->FillNtupleDColumn(2, m_totalEdep[2]);
  // analysisManager->FillNtupleDColumn(3, m_totalEdep[3]);
  analysisManager->AddNtupleRow();  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
