#include "SiliconHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include <iomanip>

G4ThreadLocal G4Allocator<SiliconHit>* SiliconHitAllocator = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiliconHit::SiliconHit()
 : G4VHit(),
   fEdep(0.),
   fCopyNumber(-1)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiliconHit::~SiliconHit() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SiliconHit::SiliconHit(const SiliconHit& right)
  : G4VHit()
{
  fCopyNumber  = right.fCopyNumber;
  fEdep        = right.fEdep;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const SiliconHit& SiliconHit::operator=(const SiliconHit& right)
{
  fEdep        = right.fEdep;
  fCopyNumber  = right.fCopyNumber;

  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4int SiliconHit::operator==(const SiliconHit& right) const
{
  return ( this == &right ) ? 1 : 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SiliconHit::Print()
{
}

