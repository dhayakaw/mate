#include "DetectorConstruction.hh"
#include "SiliconSD.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4GlobalMagFieldMessenger.hh"
#include "G4AutoDelete.hh"

#include "G4SDManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4UserLimits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
 : G4VUserDetectorConstruction(),
   fCheckOverlaps(true)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ 
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Define materials 
  DefineMaterials();
  
  // Define volumes
  return DefineVolumes();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::DefineMaterials()
{ 
  G4NistManager* man = G4NistManager::Instance();
  
  G4bool isotopes = false;
  
  G4Element* Si = man->FindOrBuildElement("Si", isotopes);
  G4Element* Pb = man->FindOrBuildElement("Pb", isotopes);  
  
  G4Material* Lead = new G4Material("Lead", 11.34*g/cm3, 1);
  Lead->AddElement(Pb, 1);
  
  G4Material* Silicon = new G4Material("Silicon", 2.33*g/cm3, 1);
  Silicon->AddElement(Si, 1);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::DefineVolumes()
{
  // Gamma detector Parameters
  //
  G4double board_dX    = 10*mm, board_dY    = 10*mm,  board_dR    = 2*mm;
  G4double lead_dX     = 5*mm,  lead_dY     = 5*mm,   lead_dR     = 50*um;
  G4double detector_dX = 4*mm,  detector_dY = 1.5*mm, detector_dR= 100*um;
  G4int nb_lead = 2;
  G4int nb_board = nb_lead;
  //
  G4double dPhi = twopi/nb_lead;
  // 
  G4double ring_R = 1.5/2*cm;
  // G4double ring_R = 3.0/2*cm;
  // G4double ring_R = 3.5/2*cm;
  // G4double ring_R = 4.0/2*cm;
  //
  G4NistManager* nist = G4NistManager::Instance();
  G4Material* default_mat = nist->FindOrBuildMaterial("G4_AIR");
  G4Material* case_mat    = nist->FindOrBuildMaterial("G4_POLYSTYRENE");
  G4Material* board_mat   = nist->FindOrBuildMaterial("G4_POLYSTYRENE");
  G4Material* lead_mat    = nist->FindOrBuildMaterial("Lead");
  G4Material* detector_mat= nist->FindOrBuildMaterial("Silicon");
        
  //     
  // World
  //
  G4double world_sizeXY = 5.*ring_R;
  // G4double world_sizeZ  = 1.2*board_dY;
  G4double world_sizeZ  = 4*board_dY;
  
  G4Box* solidWorld =    
    new G4Box("World",                       //its name
       0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ); //its size
      
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        default_mat,         //its material
                        "World");            //its name
                                   
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
                 
  // define board
  G4Box* solidBoard = new G4Box("board", board_dR/2, board_dX/2, board_dY/2);
                     
  G4LogicalVolume* logicBoard = 
    new G4LogicalVolume(solidBoard,          //its solid
                        board_mat,           //its material
                        "BoardLV");        //its name
               
  // place board
  for (G4int icrys = 0; icrys < nb_board ; icrys++) {
    G4double phi = icrys*dPhi;
    G4RotationMatrix rotm  = G4RotationMatrix();
    rotm.rotateZ(phi);
    G4ThreeVector uz = G4ThreeVector(std::cos(phi),  std::sin(phi),0.);     
    G4ThreeVector position = (ring_R+0.5*board_dR)*uz;
    G4Transform3D transform = G4Transform3D(rotm,position);
                                    
    new G4PVPlacement(transform,             //rotation,position
                      logicBoard,            //its logical volume
                      "board",             //its name
                      logicWorld,             //its mother  volume
                      false,                 //no boolean operation
                      icrys,                 //copy number
                      fCheckOverlaps);       // checking overlaps 
  }
  // for (G4int icrys = 0; icrys < nb_board ; icrys++) {
    // G4double phi = icrys*dPhi;
    // G4RotationMatrix rotm  = G4RotationMatrix();
    // rotm.rotateZ(phi);
    // G4ThreeVector uz = G4ThreeVector(std::cos(phi),  std::sin(phi),0.);     
    // G4ThreeVector position = (ring_R+0.5*board_dR+10.)*uz; // 2nd
    // G4Transform3D transform = G4Transform3D(rotm,position);
                                    
    // new G4PVPlacement(transform,             //rotation,position
                      // logicBoard,            //its logical volume
                      // "board",             //its name
                      // logicWorld,             //its mother  volume
                      // false,                 //no boolean operation
                      // icrys+2,                 //copy number
                      // fCheckOverlaps);       // checking overlaps 
  // }
                    
  // define lead tape
  G4Box* solidLead = new G4Box("lead", lead_dR/2, lead_dX/2, lead_dY/2);
                     
  G4LogicalVolume* logicLead = 
    new G4LogicalVolume(solidLead,          //its solid
                        lead_mat,           //its material
                        "LeadLV");        //its name
               
  // place lead tapes
  for (G4int icrys = 0; icrys < nb_lead ; icrys++) {
    G4double phi = icrys*dPhi;
    G4RotationMatrix rotm  = G4RotationMatrix();
    rotm.rotateZ(phi);
    G4ThreeVector uz = G4ThreeVector(std::cos(phi),  std::sin(phi),0.);     
    G4ThreeVector position = (ring_R+board_dR+0.5*lead_dR)*uz;
    G4Transform3D transform = G4Transform3D(rotm,position);
                                    
    new G4PVPlacement(transform,              //rotation,position
                      logicLead,              //its logical volume
                      "lead",                 //its name
                      logicWorld,             //its mother  volume
                      false,                  //no boolean operation
                      icrys,                  //copy number
                      fCheckOverlaps);        // checking overlaps 
  }
  // for (G4int icrys = 0; icrys < nb_lead ; icrys++) {
    // G4double phi = icrys*dPhi;
    // G4RotationMatrix rotm  = G4RotationMatrix();
    // rotm.rotateZ(phi);
    // G4ThreeVector uz = G4ThreeVector(std::cos(phi),  std::sin(phi),0.);     
    // G4ThreeVector position = (ring_R+board_dR+0.5*lead_dR+10.)*uz; // 2nd
    // G4Transform3D transform = G4Transform3D(rotm,position);
                                    
    // new G4PVPlacement(transform,              //rotation,position
                      // logicLead,              //its logical volume
                      // "lead",                 //its name
                      // logicWorld,             //its mother  volume
                      // false,                  //no boolean operation
                      // icrys+2,                  //copy number
                      // fCheckOverlaps);        // checking overlaps 
  // }
                                                      
  // full detector
  G4Box* solidDetector =
    new G4Box("detector", detector_dR/2, detector_dX/2, detector_dY/2);
      
  G4LogicalVolume* logicDetector =                         
    new G4LogicalVolume(solidDetector,       //its solid
                        detector_mat,         //its material
                        "DetectorLV");         //its name
                                 
  // place detectors 
  //
  for (G4int icrys = 0; icrys < nb_lead ; icrys++) {
    G4double phi = icrys*dPhi;
    G4RotationMatrix rotm  = G4RotationMatrix();
    rotm.rotateZ(phi);
    G4ThreeVector uz = G4ThreeVector(std::cos(phi),  std::sin(phi),0.);     
    G4ThreeVector position = (ring_R+board_dR+lead_dR+0.5*detector_dR)*uz;
    G4Transform3D transform = G4Transform3D(rotm,position);
                                    
    new G4PVPlacement(transform,             //rotation,position
                      logicDetector,            //its logical volume
                      "detector",             //its name
                      logicWorld,             //its mother  volume
                      false,                 //no boolean operation
                      icrys,                 //copy number
                      fCheckOverlaps);       // checking overlaps 
  }
  // for (G4int icrys = 0; icrys < nb_lead ; icrys++) {
    // G4double phi = icrys*dPhi;
    // G4RotationMatrix rotm  = G4RotationMatrix();
    // rotm.rotateZ(phi);
    // G4ThreeVector uz = G4ThreeVector(std::cos(phi),  std::sin(phi),0.);     
    // G4ThreeVector position = (ring_R+board_dR+lead_dR+0.5*detector_dR+10.)*uz; // 2nd
    // G4Transform3D transform = G4Transform3D(rotm,position);
                                    
    // new G4PVPlacement(transform,             //rotation,position
                      // logicDetector,            //its logical volume
                      // "detector",             //its name
                      // logicWorld,             //its mother  volume
                      // false,                 //no boolean operation
                      // icrys+2,                 //copy number
                      // fCheckOverlaps);       // checking overlaps 
  // }

  //
  // case
  //
  G4double case_radius = 12.5*mm;
  G4double case_dZ = 3*mm;  
    
  G4Tubs* solidCase =
    new G4Tubs("case", 0., case_radius, 0.5*case_dZ, 0., twopi);
      
  G4LogicalVolume* logicCase =                         
    new G4LogicalVolume(solidCase,        //its solid
                        case_mat,         //its material
                        "CaseLV");        //its name
               
  //
  // place case in world
  //                    
  //
  G4RotationMatrix* myRotation = new G4RotationMatrix();
  myRotation->rotateX(0.*deg);
  myRotation->rotateY(90.*deg);
  myRotation->rotateZ(0.*rad);
  // new G4PVPlacement(0,                       //no rotation
  new G4PVPlacement(myRotation,              //my rotation
                    G4ThreeVector(),         //at (0,0,0)
                    logicCase,               //its logical volume
                    "case",                  //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    fCheckOverlaps);         // checking overlaps 
  

  // Visualization attributes
  //
  logicLead    ->SetVisAttributes (G4VisAttributes(G4Color (1., 0., 0.)));
  logicDetector->SetVisAttributes (G4VisAttributes(G4Color (0., 0., 1.)));    
  G4VisAttributes * caseVisAtt = new G4VisAttributes(G4Colour(1.,1.,1.));
  caseVisAtt->SetForceWireframe(true);
  logicCase->SetVisAttributes (caseVisAtt);
  G4VisAttributes * worldVisAtt = new G4VisAttributes(G4Colour(1.,1.,1.));
  worldVisAtt->SetForceWireframe(true);
  logicWorld->SetVisAttributes (worldVisAtt);

  // Print materials
  G4cout << *(G4Material::GetMaterialTable()) << G4endl; 

  //
  // Always return the physical World
  //
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  auto  aSiliconSD = new SiliconSD( "SiliconSD", "SiliconHitsCollection" );
  SDman->AddNewDetector( aSiliconSD );
  SetSensitiveDetector( "DetectorLV", aSiliconSD );                                        
}

