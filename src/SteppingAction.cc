
#include "SteppingAction.hh"
#include "EventAction.hh"
#include "Analysis.hh"

#include "G4RunManager.hh"
#include "G4SteppingManager.hh"
#include "G4VProcess.hh"
#include "G4UnitsTable.hh"

// G4int nGamma=0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* event)
:G4UserSteppingAction(), fEventAction(event)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* aStep)
{
  if(aStep->GetTrack()->GetDefinition()->GetParticleName()=="gamma"){
    if(aStep->GetPreStepPoint()->GetTouchable()->GetCopyNumber()!=6){
      if(aStep->GetPostStepPoint()->GetTouchable()->GetCopyNumber()==6){
        // get analysis manager
        auto analysisManager = G4AnalysisManager::Instance();
        // nGamma++;
        // G4cout << "number of gamma rays: " <<  nGamma << G4endl;
        analysisManager->FillNtupleDColumn(2, aStep->GetPreStepPoint()->GetTotalEnergy());
        analysisManager->AddNtupleRow();
      }
    }
  }


/*  
  //debug: charge and mass
  //
  G4int stepNb = aStep->GetTrack()->GetCurrentStepNumber();
  G4StepPoint* postPoint = aStep->GetPostStepPoint();
  G4double charge = postPoint->GetCharge();
  G4double mass   = postPoint->GetMass();
  G4cout << "\n   step= " << stepNb << "   charge= " << charge 
         << "  mass= " << G4BestUnit(mass, "Energy");
*/      
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

